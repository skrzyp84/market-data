def kubectl(namespace, command) {
  container('kubectl') {
    sh("kubectl --namespace=${namespace} ${command}")
  }
}

def toKubernetes(tagToDeploy, namespace, deploymentName) {
  sh("sed -i.bak 's#BUILD_TAG#${tagToDeploy}#' ./deploy/${namespace}/*.yml")

  //kubectl("apply -f deploy/${namespace}/")
  container('kubectl') {
    sh("kubectl --namespace=${namespace} apply -f deploy/${namespace}/")
  }
}

def rollback(deploymentName) {
  //kubectl("rollout undo deployment/${deploymentName}")
  container('kubectl') {
    sh("kubectl --namespace=${namespace} rollout undo deployment/${deploymentName}")
  }
}

return this;